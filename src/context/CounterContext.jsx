import React, { useReducer, createContext } from 'react';

const counterReducer = (state, action) => {
  switch (action.type) {
    case 'inc':
      return ({ ...state, count: state.count + 1 });
    case 'dec':
      return ({ ...state, count: state.count - 1 });
    case 'inc+':
      return ({ ...state, count: state.count + action.payload });
    default:
      return ({ ...state });
  }
};

const CounterContext = createContext();

const CounterProvider = ({ children }) => {
  const [state, dispatch] = useReducer(counterReducer, { count: 0 });

  const inc = () => {
    dispatch({ type: 'inc' });
  }
  const dec = () => {
    dispatch({ type: 'dec' });
  }
  const incPlus = (x) => {
    dispatch({ type: 'inc+', payload: x });
  }

  return (
    <CounterContext.Provider value={{ state, inc, dec, incPlus }}>
      {children}
    </CounterContext.Provider>
  )
}

export { CounterContext, CounterProvider };
