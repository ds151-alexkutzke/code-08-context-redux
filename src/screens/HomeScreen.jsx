import React, { useReducer, useContext } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
import { CounterContext } from '../context/CounterContext';

const HomeScreen = ({ navigation }) => {
  const counterContext = useContext(CounterContext);

  return (
    <View>
      <Text>Contador do CounterProvider: {counterContext.state.count}</Text>
      <Button
        title="Incrementa"
        onPress={() => { counterContext.inc() }}
      />
      <Button
        title="Decrementa"
        onPress={() => { counterContext.dec() }}
      />
      <Button
        title="Incrementa mais"
        onPress={() => { counterContext.incPlus(5) }}
      />
    </View>
  )
}

const styles = StyleSheet.create({});

export default HomeScreen;
